from django.contrib.auth import logout
from django.shortcuts import render
from django.http import HttpResponseRedirect, response, JsonResponse, HttpResponse
from django.urls import reverse
import json

from .forms import formStatus, formRegistrasi
from .models import formModels, formRegistrasiModels
import requests
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
#diary_dict = {}
def index(request):
    isiForms = formModels.objects.all()
    response = {'forms' : formStatus, 'result': isiForms}
    return render(request, "lab6.html", response)

def forms_post(request):
    response = {}
    if(request.method == "POST" or None):
        response['status'] = request.POST['status']
        forms = formModels(status=response['status'])
        forms.save()
        return HttpResponseRedirect(reverse('index:index'))
    else:
        return HttpResponseRedirect('/')
        
def myProfile(request):
    return render(request, 'myProfile.html')

def myLibrary(request):
    return render(request, 'myLibrary.html')

def jsonLibrary(request, kategori=None):
    url = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + str(kategori))
    urlJson = url.json()
    return JsonResponse(urlJson)

def registrasi(request):
    response = {}
    if(request.method == "POST" or None):
        response['nama'] = request.POST['nama']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        forms = formRegistrasiModels(nama=response['nama'], email=response['email'], password=response['password'])
        forms.save()
    return JsonResponse({})

def registrationRender(request):
    isiForms = formRegistrasiModels.objects.all()
    response = {'forms' : formRegistrasi, 'result_registrasi': isiForms}
    return render(request, "registrasi.html", response)

def validate_email(request):
    email = request.POST.get('email', None)
    data = {
        'is_taken': formRegistrasiModels.objects.filter(email=email).exists()
    }
    return JsonResponse(data)

def registrationData(request):
    isiForms = formRegistrasiModels.objects.all()
    sub_list = [{'nama' : e.nama, 'email' : e.email} for e in isiForms]
    return HttpResponse(json.dumps({'data': sub_list}))

def unSubscribe(request):
    email = request.POST.get('email')
    formRegistrasiModels.objects.filter(email=email).delete()
    return HttpResponse("x")

def logOutViews(request):
    logout(request)
    return HttpResponseRedirect("/my-library")






# My Library Ga jadi :) #
# def myLibrary(request):
#     titles = list()
#     url = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
#     urlJson = url.json()
#     response['buku'] = urlJson
#     for i in range (urlJson):
#         titles.append(urlJson['items'][i]['volumeInfo']['title'])
#     return render(request, 'myLibrary.html', response)