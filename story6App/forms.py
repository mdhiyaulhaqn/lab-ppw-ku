from django import forms

class formStatus(forms.Form):
    attrs = {'class': 'form-control'}
    status = forms.CharField(label='Status', required=True, max_length=300, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))

class formRegistrasi(forms.Form):
    nama = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs={'class' : 'form-control',
                                                                                                     'placeholder' : 'Nama'}))
    email = forms.EmailField(label='', required=True, max_length=30, widget=forms.TextInput(attrs={'class' : 'form-control',
                                                                                                       'type' : 'email',
                                                                                                       'placeholder' : 'Alamat Email'}))
    password = forms.CharField(label='', required=True, max_length=30,  widget=forms.TextInput(attrs={'class' : 'form-control',
                                                                                                              'type' : 'password',
                                                                                                              'placeholder' : 'Password'}))