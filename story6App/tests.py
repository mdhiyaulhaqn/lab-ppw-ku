from django.test import TestCase, Client
from django.urls import resolve
from .views import index, myProfile
from .models import formModels
from django.http import HttpRequest

class story6UnitTest(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story_6_returns_correct_html(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        self.assertIn('<h1>Hello Apa kabar?</h1>', html)

    def test_story_6_url_is_not_exist(self):
        response = Client().get('/GakAdaaaa')
        self.assertEqual(response.status_code, 404)

    def test_story_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_6_using_lab_6_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'lab6.html')

    def test_model_can_create_status(self):
        #Creating a new activity
        status = formModels.objects.create(status="Tolong saya belajar PPW :')")

        #Retrieving all available activity
        counting_all_status = formModels.objects.all().count()
        self.assertEqual(counting_all_status,1)


    def test_my_profile_is_exist(self):
        response = Client().get('/my-profile')
        self.assertEqual(response.status_code, 200)

    def test_my_profile_using_my_profile_template(self):
        response = Client().get('/my-profile')
        self.assertTemplateUsed(response, 'myProfile.html')

    def test_story_6_using_myProfile_func(self):
        found = resolve('/story6App/my-profile')
        self.assertEqual(found.func, myProfile)
    
    def test_my_profile_returns_correct_html(self):
        request = HttpRequest()
        response = myProfile(request)
        html = response.content.decode('utf8')
        self.assertIn('<img src="https://pbs.twimg.com/profile_images/1042423648437469185/N0XYJ4W2_400x400.jpg" class="gambar rounded-circle img-fluid">', html)
        self.assertIn('<h5>Nama : Muhammad Dhiyaulhaq Nugraha</h5>', html)
        self.assertIn('<h5>Tanggal Lahir : 4 November 1999</h5>', html)
        self.assertIn('<h5>Hobi : Badminton</h5>', html)