// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}







function showpage(){
    document.getElementById("preload").style.display = "none";
    document.getElementById("content").style.display = "block";
}

$( function() {
    $( "#accordion" ).accordion({
        active: false,
        collapsible: true,
        heightStyle: 'content'
    });
} );

var clicked = false;
var countFav = 0;
$(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $("#tombol").click(function(){
        if(!clicked) {
            $("body").css('background-color', '#7D7D7D'),
                $("h5").css('color', 'white'),
                $("h5").css('font-family', 'Times'),
                $(".navbar").css('background-image', 'linear-gradient(to right, black, dimgrey)'),
                $(".nav-link").css('color', 'white'),
                $(".ui-accordion-header").css('background-image', 'linear-gradient(to right, white, lightgrey)'),
                $("#tombol").attr('class', 'btn btn-secondary')
            clicked = true;
        } else {
            $("body").css('background-color', 'white'),
                $(".navbar").css('background-color', '#6DDCBD'),
                $("h5").css('color', 'black'),
                $("h5").css('font-family', 'Times'),
                $(".navbar").css('background-image', 'linear-gradient(to right, deepskyblue, dodgerblue)'),
                $(".nav-link").css('color', 'black'),
                $(".ui-accordion-header").css('background-image', 'linear-gradient(to right, deepskyblue, dodgerblue)'),
                $("#tombol").attr('class', 'btn btn-primary')
            clicked = false;
        }
    });


    $.ajax("/json-library/quilting")
        .done(function(isi){
            var hasil = '';
            for(var i=0; i < isi.items.length; i++){
                var judul = isi.items[i].volumeInfo.title;
                var gambar = isi.items[i].volumeInfo.imageLinks.thumbnail;
                var penulis = isi.items[i].volumeInfo.authors[0];
                var deskripsi = isi.items[i].volumeInfo.description;
                var idnum = "" + isi.items[i].id;
                hasil += '<tr>' +
                    '<td><img src="'+ gambar +'"></td>' +
                    '<td>' + judul + '</td>' +
                    '<td>'+ penulis +'</td>' +
                    '<td>'+ deskripsi +'</td>' +
                    '<td><button class="btn tombol-favorit" id="'+ idnum +'" onclick="favorite(\''+ idnum +'\')"><i class="fa fa-star"></i></button></td>' + //onclick="favorite()"
                    '</tr>';
                // console.log(idnum);
            }
            $(hasil).appendTo("#list-buku");
        });
});


function changeCategory(category){
    console.log(category);
    $.ajax("/json-library/" + category)
        .done(function(isi){
            // Hapus dulu yang sebelumnya
            countFav = 0;
            console.log("Counter Favorite : " + countFav);
            $('#jumlah-favorit').html('Jumlah buku favorit : ' + countFav);

            $('#list-buku').html("<tr id=\"table-header\">\n" +
                "<th>Sampul</th>\n" +
                "<th>Judul</th>\n" +
                "<th>Penulis</th>\n" +
                "<th>Deskripsi</th>\n" +
                "<th>Favorit</th>\n" +
                "</tr>");

            var hasil = '';
            for(var i=0; i < isi.items.length; i++){
                var judul = isi.items[i].volumeInfo.title;
                var gambar = isi.items[i].volumeInfo.imageLinks.thumbnail;
                var penulis = isi.items[i].volumeInfo.authors[0];
                var deskripsi = isi.items[i].volumeInfo.description;
                var idnum = "" + isi.items[i].id;
                hasil += '<tr>' +
                    '<td><img src="'+ gambar +'"></td>' +
                    '<td>' + judul + '</td>' +
                    '<td>'+ penulis +'</td>' +
                    '<td>'+ deskripsi +'</td>' +
                    '<td><button class="btn tombol-favorit" id="'+ idnum +'" onclick="favorite(\''+ idnum +'\')"><i class="fa fa-star"></i></button></td>' + //onclick="favorite()"
                    '</tr>';
                // console.log(idnum);
            }
            $(hasil).appendTo("#list-buku");
        });
}


function favorite(idnum) {
    var thisId = '#' + idnum;

    var bg = $(thisId).css('background-color');
    console.log(bg);

    if(bg === 'rgb(128, 128, 128)' || bg === 'rgb(169, 169, 169)'){
        //console.log($(thisId).css('background-color', 'rgb(30, 144, 255)'));
        console.log($(thisId).css('background-color', 'DodgerBlue'));
        $(thisId).css('color', 'yellow');
        countFav++;
    }
    else if(bg === 'rgb(30, 144, 255)'){
         console.log($(thisId).css('background-color', 'rgb(128, 128, 128)'));
         $(thisId).css('color', 'white');
         countFav--;
    }
    console.log("Counter Favorite : " + countFav);
    $('#jumlah-favorit').html('Jumlah buku favorit : ' + countFav);
}

var is_unique = false;
$(function() {
    showSubscriber(); // Tampilkan list subscriber pertama kali

    $("#id_email").change(function () {
        console.log($(this).val());
        email = $(this).val();
        $.ajax({
            method: "POST",
            url: '{% url "story6App:validate_email" %}',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.is_taken) {
                    alert("Email tersebut sudah pernah terdaftar. \nSilahkan gunakan email lain !");
                    is_unique = false;
                } else {
                    is_unique = true;
                }
            }
        });
    });

    $('#id_nama').on('keyup', function () {
        check_submit_button();
    });
    $('#id_email').on('keyup', function () {
        check_submit_button();
    });
    $('#id_password').on('keyup', function () {
        check_submit_button();
    });
});

function check_submit_button(){
    var panjang_nama = $('#id_nama').val().length;
    var panjang_email = $('#id_email').val().length;
    var panjang_password = $('#id_password').val().length;

    if(panjang_nama > 0 && panjang_email > 0 && panjang_password > 0 && is_unique) {
        $('#btn_submit').prop('disabled', false);
    } else {
        $('#btn_submit').prop('disabled', true);
    }
}

function submit_post() {
    console.log("create post is working!"); // sanity check
    $.ajax({
        url : "/registrasi_post/", // the endpoint
        type : "POST", // http method
        data : { nama : $('#id_nama').val(), email : $('#id_email').val(), password : $('#id_password').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            $('#id_nama').val(''); // remove the value from the input
            $('#id_email').val(''); // remove the value from the input
            $('#id_password').val(''); // remove the value from the input

            console.log(json);
            console.log("success");
            $('#btn_submit').prop('disabled', true);
            showSubscriber();
        },
    });
}


function showSubscriber(){
    $.ajax({
        url : "/registration-data",
        dataType : 'json'
    })
        .done(function(isi){
            var hasil = '';

            $('#list-subscriber').html("<tr id=\"table-header\">\n" +
                "<th>Nama</th>\n" +
                "<th>Email</th>\n" +
                "<th>Unsubscribe</th>\n" +
                "</tr>");

            for(var i=0; i < isi.data.length; i++){
                var nama = isi.data[i].nama;
                var email = isi.data[i].email;
                hasil += '<tr id="'+ email +'">' +
                    '<td>' + nama + '</td>' +
                    '<td>'+ email +'</td>' +
                    '<td><button type="button" class="btn btn-danger" id="'+ email +'" onclick="unSubscribe(\''+ email +'\')">unsubscribe</button></td>' +
                    '</tr>';
            }
            $(hasil).appendTo("#list-subscriber");
        });
}

function unSubscribe(email) {
    var thisId = '#' + email;
    console.log(thisId);
    deleteUnSubscribe(email);
}

function deleteUnSubscribe(email) {
    var objHapus = document.getElementById(email);
    objHapus.remove();

    $.ajax({
        method: "POST",
        url: '/unsubscribe/',
        data: {
            'email': email
        },
        dataType: 'json'
    });
}