from django.conf.urls import url
from django.urls import path

from .views import index, forms_post, myProfile, myLibrary, jsonLibrary, registrasi, registrationRender, validate_email, registrationData, unSubscribe, logOutViews

app_name = 'story6App'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^forms_post', forms_post, name='forms_post'),
    url(r'^my-profile', myProfile, name='challenge6'),
    url(r'^my-library', myLibrary, name='myLibrary'),
    # url(r'^json-library', jsonLibrary, name='jsonLibrary'),
    path('json-library/<kategori>', jsonLibrary, name='jsonLibrary'),
    url(r'^registrasi_post', registrasi, name='registrasi_post'),
    url(r'^registrasi', registrationRender, name='registrasi'),
    url(r'validate_email', validate_email, name='validate_email'),
    url(r'^registration-data', registrationData, name='registration-data'),
    url(r'^unsubscribe', unSubscribe, name='unSubscribe'),
    url(r'^log-out', logOutViews, name='logout')
]