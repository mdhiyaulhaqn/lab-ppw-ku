from django.contrib import admin
from  .models import formModels, formRegistrasiModels
# Register your models here.
admin.site.register(formModels)
admin.site.register(formRegistrasiModels)
